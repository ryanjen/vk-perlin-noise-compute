cmake_minimum_required(VERSION 3.0)

set(CMAKE_C_COMPILER clang)
set(CMAKE_CXX_COMPILER clang++)
project(vk-perlin-noise-compute)

set(CMAKE_EXPORT_COMPILE_COMMANDS 1)
set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} "-Wall -g")

find_library(LIBDL dl )

file(GLOB SOURCE_LIBRARIES "src/*.cpp" )
add_library(PROJ_LIBRARY ${SOURCE_LIBRARIES} )

message(STATUS ${LIBDL} )

add_executable(main src/main.cpp)

target_link_libraries(main PROJ_LIBRARY ${LIBDL} )
