#version 450

layout (local_size_x = 32, local_size_y = 32 ) in;

layout (set = 0, binding = 0, r32f) uniform writeonly image2D storageImage;

layout (constant_id = 0) const uint SEED = 0;

shared vec2 tl;  // top left
shared vec2 tr;
shared vec2 bl;
shared vec2 br;

// Adapted from wikipedia

// compute the gradient for a grid corner pseudorandomly
vec2 hash22(vec2 v) {
    uint w = 8 * 4;
    uint s = w / 2; // rotation width
    uint a = int(v.x), b = int(v.y);
    a *= 3284157443; b ^= a << s | a >> w-s;
    b *= 1911520717; a ^= b << s | b >> w-s;
    a *= 2048419325;
    float random = a * (3.14159265 / ~(~0u >> 1)); // in [0, 2*Pi]
    vec2 u;
    u.x = sin(random); u.y = cos(random);
    return u;
}


// Dot a point's offset from a corner with the gradient of that corner
float dotCorner(vec2 off, vec2 cornerPos, vec2 grad) {
  return dot(cornerPos - off, grad);
}

// smoothing function by Perlin
float fade(float t) {
  return t * t * t * (t * (t * 6 - 15) + 10);
}

void main() {
  if (gl_LocalInvocationIndex == 0) {
    tl = hash22(gl_WorkGroupID.xy);
    tr = hash22(gl_WorkGroupID.xy + vec2(1.0, 0.0));
    bl = hash22(gl_WorkGroupID.xy + vec2(0.0, 1.0));
    br = hash22(gl_WorkGroupID.xy + vec2(1.0, 1.0));
  }
  barrier();

  // interpolate dot products between this cell's offset within workgroup and each corner vectors
  float wx = float(gl_LocalInvocationID.x) / (gl_WorkGroupSize.x - 1);
  float wy = float(gl_LocalInvocationID.y) / (gl_WorkGroupSize.y - 1);
  vec2 off = vec2(wx, wy);
  
  // smooth 
  wx = fade(wx);
  wy = fade(wy);

  // weigh rightmost vector by wx
  float interpX1 = mix(dotCorner(off, vec2(0.0, 0.0), tl), dotCorner(off, vec2(1.0, 0.0), tr), wx);
  float interpX2 = mix(dotCorner(off, vec2(0.0, 1.0), bl), dotCorner(off, vec2(1.0, 1.0), br), wx);
  
  float color = mix(interpX1, interpX2, wy);

  imageStore(storageImage, ivec2(gl_GlobalInvocationID.xy), vec4(color));
}