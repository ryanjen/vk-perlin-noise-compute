#include <cstdlib>
#include <dlfcn.h>
#include <vulkan/vulkan_core.h>
#define VK_NO_PROTOTYPES 1
#include "VulkanFunctions.h"

#include <vector>
#include <cassert>
#include <cstring>
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wconstant-conversion"
#include "stb_image_write.h"
#pragma GCC diagnostic pop

#define VK_ASSERT( expr ) assert( VK_SUCCESS == (expr) );

namespace VkPerlin {

static const std::vector<const char *> desiredInstanceLayers = {
  "VK_LAYER_KHRONOS_validation"
};

static std::vector<const char *> desiredInstanceExtensions = {};
static std::vector<const char *> desiredDeviceExtensions = {};

static const VkExtent2D image_size = { 640, 480 };
static const VkExtent2D workgroup_size = { 32, 32 };

static const VkFormat image_format = VK_FORMAT_R32_SFLOAT;

class Context {
  public:
    Context();
  
    void run();
    void shutdown();

  private:
    void * library;
    VkInstance instance;
    VkPhysicalDevice physicalDevice;
    uint32_t computeFamily;
    VkDevice logicalDevice;
    VkQueue computeQueue;
    uint32_t randomSeed;

    VkPhysicalDeviceMemoryProperties memoryProperties;

    VkPipelineLayout pipelineLayout;
    VkPipeline computePipeline;

    VkImage deviceImage;
    VkDeviceMemory deviceImageMemory;
    VkImageView deviceImageView;
    VkImageSubresourceRange deviceSubresourcesRange;

    VkImage hostImage;
    VkDeviceMemory hostImageMemory;

    VkDescriptorPool descriptorPool;
    VkDescriptorSetLayout imageDescriptorSetLayout;
    VkDescriptorSet imageDescriptorSet;

    void loadGlobalFunctions() {
      this->library = dlopen( "libvulkan.so.1", RTLD_NOW );
      assert(this->library != nullptr);

      // load loader function from vulkan dynamic library
      #define EXPORTED_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) dlsym( this->library, #name ); \
        assert( name != nullptr );

      // load global level functions
      #define GLOBAL_LEVEL_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) vkGetInstanceProcAddr( nullptr, #name ); \
        assert( name != nullptr);

      #include "ListOfVulkanFunctions.inl"
    }


    void createInstance() {
      //VkPhysicalDeviceFeatures desiredDeviceFeatures = {};

      VkApplicationInfo applicationInfo = {
        VK_STRUCTURE_TYPE_APPLICATION_INFO,
        nullptr,
        "vk triangle",
        VK_MAKE_VERSION(1, 0, 0),
        "triangle engine",
        VK_MAKE_VERSION(1, 0, 0),
        VK_MAKE_VERSION(1, 0, 0)
      };

      VkInstanceCreateInfo instanceCreateInfo = {
        VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        nullptr,
        0,
        &applicationInfo,
        (uint32_t) desiredInstanceLayers.size(),
        desiredInstanceLayers.data(),
        (uint32_t) desiredInstanceExtensions.size(),
        desiredInstanceExtensions.size() > 0 ? &desiredInstanceExtensions[0] : nullptr
      };

      VK_ASSERT( vkCreateInstance(&instanceCreateInfo, nullptr, &this->instance));
    }

    void loadInstanceFunctions() {
      #define INSTANCE_LEVEL_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) vkGetInstanceProcAddr( this->instance, #name ); \
        assert(name != nullptr);
      
      #include "ListOfVulkanFunctions.inl"

      #define INSTANCE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) \
        for (auto & enabled : desiredInstanceExtensions ) { \
          if ( !strcmp(enabled, extension ) ) { \
            name = (PFN_##name) vkGetInstanceProcAddr( this->instance, #name); \
            assert(name != nullptr); \
          } \
        }
      
      #include "ListOfVulkanFunctions.inl"
    }

    bool selectComputeFamily(VkPhysicalDevice device) {
      std::vector<VkQueueFamilyProperties> families;
      uint32_t numFamilies;
      vkGetPhysicalDeviceQueueFamilyProperties(device, &numFamilies, nullptr);
      families.resize(numFamilies);
      vkGetPhysicalDeviceQueueFamilyProperties(device, &numFamilies, families.data());

      for (uint32_t i = 0; i < families.size(); i++) {
        if (families[i].queueFlags & VK_QUEUE_COMPUTE_BIT) {
          this->computeFamily = i;
          return true;
        }
      }
      return false;
    }

    void choosePhysicalDevice() {
      std::vector<VkPhysicalDevice> physicalDevices;
      uint32_t numDevices;
      vkEnumeratePhysicalDevices(this->instance, &numDevices, nullptr);
      physicalDevices.resize(numDevices);
      vkEnumeratePhysicalDevices(this->instance, &numDevices, physicalDevices.data());

      this->physicalDevice = VK_NULL_HANDLE;
      for (auto device: physicalDevices) {
        if (selectComputeFamily(device)) {
          this->physicalDevice = device;
          return;
        }
      }
      assert(this->physicalDevice != VK_NULL_HANDLE);
    }

    void createLogicalDevice() {
      float prio[] = { 1.0f }; 
      VkDeviceQueueCreateInfo queueInfo = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .queueFamilyIndex = this->computeFamily,
        .queueCount = 1,
        .pQueuePriorities = prio,
      };

      VkPhysicalDeviceFeatures deviceFeatures = {};
      VkDeviceCreateInfo deviceCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .queueCreateInfoCount = 1,
        .pQueueCreateInfos = &queueInfo,
        .enabledLayerCount = 0,
        .ppEnabledLayerNames = nullptr,
        .enabledExtensionCount = (uint32_t) desiredDeviceExtensions.size(),
        .ppEnabledExtensionNames = desiredDeviceExtensions.data(),
        .pEnabledFeatures = &deviceFeatures,
      };
      VK_ASSERT( vkCreateDevice(this->physicalDevice, &deviceCreateInfo, nullptr, &this->logicalDevice) );
    };

    void loadDeviceFunctions() {
      #define DEVICE_LEVEL_VULKAN_FUNCTION( name ) \
        name = (PFN_##name) vkGetDeviceProcAddr(this->logicalDevice, #name); \
        assert(name != nullptr);

      #define DEVICE_LEVEL_VULKAN_FUNCTION_FROM_EXTENSION( name, extension ) \
        for (auto & enabled: desiredDeviceExtensions) { \
          if ( !strcmp(enabled, extension) ) { \
            name = (PFN_##name) vkGetDeviceProcAddr(this->logicalDevice, #name); \
            assert(name != nullptr); \
          } \
        }

      #include "ListOfVulkanFunctions.inl"
    }


    VkShaderModule loadShaderModule(const std::string fname) {
      std::vector<unsigned char> data;
      FILE *f = fopen(fname.c_str(), "r");
      assert(f != nullptr);
      
      fseek(f, 0, SEEK_END);
      size_t len = ftell(f);
      fseek(f, 0, SEEK_SET);

      data.resize(len);
      assert( 1 == fread(data.data(), len, 1, f) );
      
      fclose(f);

      VkShaderModuleCreateInfo moduleInfo {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .codeSize = (uint32_t) data.size(),
        .pCode = (uint32_t *) data.data()
      };

      VkShaderModule module;
      VK_ASSERT( vkCreateShaderModule(this->logicalDevice, &moduleInfo, nullptr, &module) );
      return module;
    }

    void createComputePipeline() {
      VkDescriptorSetLayoutBinding imageBinding = {
        .binding = 0,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
        .descriptorCount = 1,
        .stageFlags = VK_SHADER_STAGE_COMPUTE_BIT,
        .pImmutableSamplers = nullptr,
      };

      VkDescriptorSetLayoutCreateInfo descriptorSetLayoutInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .bindingCount = 1,
        .pBindings = &imageBinding,
      };

      VK_ASSERT( vkCreateDescriptorSetLayout(this->logicalDevice, &descriptorSetLayoutInfo, nullptr, &this->imageDescriptorSetLayout) );

      VkPipelineLayoutCreateInfo pipelineLayoutInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .setLayoutCount = 1,
        .pSetLayouts = &this->imageDescriptorSetLayout,
        .pushConstantRangeCount = 0,
        .pPushConstantRanges = nullptr
      };

      VK_ASSERT( vkCreatePipelineLayout(this->logicalDevice, &pipelineLayoutInfo, nullptr, &this->pipelineLayout) );

      VkSpecializationMapEntry seedEntry = {
        .constantID = 0,
        .offset = 0,
        .size = sizeof(this->randomSeed)
      };

      VkSpecializationInfo specInfo = {
        .mapEntryCount = 1,
        .pMapEntries = &seedEntry,
        .dataSize = sizeof(this->randomSeed),
        .pData = &this->randomSeed
      };

      VkShaderModule shaderModule = loadShaderModule("spirv/perlin_noise.comp.spv");

      VkPipelineShaderStageCreateInfo shaderInfo = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .stage = VK_SHADER_STAGE_COMPUTE_BIT,
        .module = shaderModule,
        .pName = "main",
        .pSpecializationInfo = &specInfo,
      };

      VkComputePipelineCreateInfo pipelineInfo = {
        .sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .stage = shaderInfo,
        .layout = this->pipelineLayout,
        .basePipelineHandle = nullptr,
        .basePipelineIndex = -1
      };

      VK_ASSERT( vkCreateComputePipelines(this->logicalDevice, VK_NULL_HANDLE, 1, 
              &pipelineInfo, nullptr, &this->computePipeline) );

      vkDestroyShaderModule(logicalDevice, shaderModule, nullptr);
    }


    void createImage(VkMemoryPropertyFlags desiredProperties, VkBufferUsageFlags usage, VkImageTiling tiling, VkImage & image, VkDeviceMemory & memoryObject) {
      VkImageCreateInfo imageCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = image_format,
        .extent = { image_size.width, image_size.height, 1 },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = tiling,
        .usage = usage,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = nullptr,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
      };
      VK_ASSERT( vkCreateImage(this->logicalDevice, &imageCreateInfo, nullptr, &image) );

      VkMemoryRequirements requirements;
      vkGetImageMemoryRequirements(this->logicalDevice, image, &requirements);

      memoryObject = VK_NULL_HANDLE;
      
      uint32_t type;
      for (type = 0; type < this->memoryProperties.memoryTypeCount; type++) {
        if (requirements.memoryTypeBits & (1 << type) &&
            ( ( this->memoryProperties.memoryTypes[type].propertyFlags & desiredProperties) == desiredProperties ) ) {
          VkMemoryAllocateInfo allocInfo = {
            .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
            .pNext = nullptr,
            .allocationSize = requirements.size,
            .memoryTypeIndex = type,
          };
          VkResult res = vkAllocateMemory(this->logicalDevice, &allocInfo, nullptr, &memoryObject);
          if (res == VK_SUCCESS)
            break;
        }
      }
      assert(memoryObject != VK_NULL_HANDLE);
      VK_ASSERT( vkBindImageMemory(this->logicalDevice, image, memoryObject, 0) );
    }

    
    void createStorageImageView() {
      VkComponentMapping compMap = {
        .r = VK_COMPONENT_SWIZZLE_IDENTITY,
        .g = VK_COMPONENT_SWIZZLE_IDENTITY,
        .b = VK_COMPONENT_SWIZZLE_IDENTITY,
        .a = VK_COMPONENT_SWIZZLE_IDENTITY
      };

      this->deviceSubresourcesRange = {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .baseMipLevel = 0,
        .levelCount = VK_REMAINING_MIP_LEVELS,
        .baseArrayLayer = 0,
        .layerCount = VK_REMAINING_ARRAY_LAYERS
      };

      VkImageViewCreateInfo viewInfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .image = this->deviceImage,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = image_format,
        .components = compMap,
        .subresourceRange = this->deviceSubresourcesRange
      };
      VK_ASSERT( vkCreateImageView(this->logicalDevice, &viewInfo, nullptr, &this->deviceImageView) );
    }
    

    void createImageDescriptorSet() {
      VkDescriptorPoolSize poolSize = {
        .type = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
        .descriptorCount = 1,
      };

      VkDescriptorPoolCreateInfo poolInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .maxSets = 1,
        .poolSizeCount = 1,
        .pPoolSizes = &poolSize
      };

      VK_ASSERT( vkCreateDescriptorPool(this->logicalDevice, &poolInfo, nullptr, &this->descriptorPool) );

      VkDescriptorSetAllocateInfo setAllocInfo = {
        .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
        .pNext = nullptr,
        .descriptorPool = this->descriptorPool,
        .descriptorSetCount = 1,
        .pSetLayouts = &this->imageDescriptorSetLayout
      };

      VK_ASSERT( vkAllocateDescriptorSets(this->logicalDevice, &setAllocInfo, &this->imageDescriptorSet) );
    }


    void updateDescriptorSet() {
      VkDescriptorImageInfo imageInfo = {
        .sampler = VK_NULL_HANDLE,
        .imageView = this->deviceImageView,
        .imageLayout = VK_IMAGE_LAYOUT_GENERAL
      };

      VkWriteDescriptorSet descWrite = {
        .sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
        .pNext = nullptr,
        .dstSet = this->imageDescriptorSet,
        .dstBinding = 0,
        .dstArrayElement = 0,
        .descriptorCount = 1,
        .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
        .pImageInfo = &imageInfo,
        .pBufferInfo = nullptr,
        .pTexelBufferView = nullptr
      };
      vkUpdateDescriptorSets(this->logicalDevice, 1, &descWrite, 0, nullptr);
    }


    void dispatch() {
      VkCommandPool cmdPool;
      VkCommandPoolCreateInfo poolInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .queueFamilyIndex = this->computeFamily
      };
      VK_ASSERT( vkCreateCommandPool(this->logicalDevice, &poolInfo, nullptr, &cmdPool) );

      VkCommandBuffer cmdBuffer;
      VkCommandBufferAllocateInfo allocInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
        .pNext = nullptr,
        .commandPool = cmdPool,
        .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
        .commandBufferCount = 1,
      };
      VK_ASSERT( vkAllocateCommandBuffers(this->logicalDevice, &allocInfo, &cmdBuffer) );

      VkCommandBufferBeginInfo beginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .pNext = nullptr,
        .flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT,
        .pInheritanceInfo = nullptr
      };

      /////////////// IMAGE TRANSITIONS BEFORE AND AFTER SHADER
      VkImageMemoryBarrier storageTransitionBefore = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = nullptr,
        .srcAccessMask = 0,  // I think 0 here means there will be an unconditional layout transition
                // there is not really an availablility operation, so I'm a bit unsure
        .dstAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_GENERAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = this->deviceImage,
        .subresourceRange = this->deviceSubresourcesRange
      };

      VkImageMemoryBarrier storageTransitionBeforeTransfer = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = nullptr,
        .srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_GENERAL,
        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = this->deviceImage,
        .subresourceRange = this->deviceSubresourcesRange   
      };

      VkImageMemoryBarrier hostTransitionBeforeTransfer = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = nullptr,
        .srcAccessMask = 0,
        .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
        .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = this->hostImage,
        .subresourceRange = this->deviceSubresourcesRange
      };

      // I needed an additional transition to be able to read mapped memory
      VkImageMemoryBarrier hostTransitionAfterTransfer = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
        .pNext = nullptr,
        .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
        .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
        .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        .newLayout = VK_IMAGE_LAYOUT_GENERAL,
        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
        .image = this->hostImage,
        .subresourceRange = this->deviceSubresourcesRange
      };

      std::vector<VkImageMemoryBarrier> transitionsBeforeTransfer = { storageTransitionBeforeTransfer, hostTransitionBeforeTransfer };

      VkImageSubresourceLayers subresourceLayers = {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .mipLevel = 0,
        .baseArrayLayer = 0,
        .layerCount = 1
      };
      VkImageCopy copy = {
        .srcSubresource = subresourceLayers,
        .srcOffset = { 0, 0, 0 },
        .dstSubresource = subresourceLayers,
        .dstOffset = { 0, 0, 0 },
        .extent = { image_size.width, image_size.height, 1 }  // confirmed that this size is in texels, not bytes
      };

      /////////////// BEGIN RECORDING
      vkBeginCommandBuffer(cmdBuffer, &beginInfo);

      vkCmdBindPipeline(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, this->computePipeline);

      // TODO double check this TOP_OF_PIPE, I want to include everything
      vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, 0, 0, nullptr, 0, nullptr,
              1, &storageTransitionBefore);

      vkCmdBindDescriptorSets(cmdBuffer, VK_PIPELINE_BIND_POINT_COMPUTE, this->pipelineLayout, 0, 1, 
              &this->imageDescriptorSet, 0, nullptr);

      vkCmdDispatch(cmdBuffer, image_size.width / workgroup_size.width,
                                image_size.height / workgroup_size.height, 
                                1);

      vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr,
              (uint32_t) transitionsBeforeTransfer.size(), transitionsBeforeTransfer.data());

      vkCmdCopyImage(cmdBuffer, this->deviceImage, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, this->hostImage, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &copy);

      vkCmdPipelineBarrier(cmdBuffer, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr,
              1, &hostTransitionAfterTransfer);

      /////////////// END RECORDING
      vkEndCommandBuffer(cmdBuffer);

      VkSubmitInfo submitInfo = {
        .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
        .pNext = nullptr,
        .waitSemaphoreCount = 0,
        .pWaitSemaphores = nullptr,
        .pWaitDstStageMask = nullptr, 
        .commandBufferCount = 1,
        .pCommandBuffers = &cmdBuffer,
        .signalSemaphoreCount = 0,
        .pSignalSemaphores = nullptr,
      };

      VK_ASSERT( vkQueueSubmit(this->computeQueue, 1, &submitInfo, VK_NULL_HANDLE) );

      vkDeviceWaitIdle(this->logicalDevice);
      vkResetCommandPool(this->logicalDevice, cmdPool, VK_COMMAND_POOL_RESET_RELEASE_RESOURCES_BIT);
      vkDestroyCommandPool(this->logicalDevice, cmdPool, nullptr);
    }

    void retrieveImage() {
      float *src;
      std::vector<uint8_t> dst;
      dst.resize(image_size.width * image_size.height);

      VK_ASSERT( vkMapMemory(this->logicalDevice, this->hostImageMemory, 0, VK_WHOLE_SIZE, 0, (void **)&src) );

      // TODO check that this is safe, the image is in TRANSFER_DST_OPTIMAL.
      // I think it's fine because the host image has normal tiling
      // should check that it's row major

      for (int i = 0; i < image_size.width * image_size.height; i++) {
        dst[i] = (uint8_t) (255 * src[i]); 
      }

      int res = stbi_write_jpg("test.jpg", image_size.width, image_size.height, 1, dst.data(), 0);
      assert( res > 0 );

      vkUnmapMemory(this->logicalDevice, this->hostImageMemory);
    }
};

Context::Context() {
  library = VK_NULL_HANDLE;
  instance = VK_NULL_HANDLE;
  logicalDevice = VK_NULL_HANDLE; 
}

void Context::run() {
  srand(time(NULL));
  this->randomSeed = (uint32_t) rand();

  std::cout << "seed: " << randomSeed << std::endl;

  loadGlobalFunctions();
  createInstance();
  loadInstanceFunctions();
  choosePhysicalDevice();
  createLogicalDevice();
  loadDeviceFunctions();
  vkGetDeviceQueue(logicalDevice, computeFamily, 0, &computeQueue);
  vkGetPhysicalDeviceMemoryProperties(this->physicalDevice, &this->memoryProperties);

  createImage(VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT,
          VK_IMAGE_TILING_OPTIMAL, this->deviceImage, this->deviceImageMemory);
  createImage(VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, VK_IMAGE_USAGE_TRANSFER_DST_BIT, VK_IMAGE_TILING_LINEAR, 
          this->hostImage, this->hostImageMemory);

  createStorageImageView();

  createComputePipeline();
  createImageDescriptorSet();
  updateDescriptorSet();

  dispatch();
  
  retrieveImage();
}

void Context::shutdown() {
  vkDeviceWaitIdle(logicalDevice);
  vkDestroyImageView(logicalDevice, deviceImageView, nullptr);
  vkDestroyImage(logicalDevice, deviceImage, nullptr);  
  vkDestroyImage(logicalDevice, hostImage, nullptr);
  vkFreeMemory(logicalDevice, deviceImageMemory, nullptr);
  vkFreeMemory(logicalDevice, hostImageMemory, nullptr);
  vkDestroyPipeline(logicalDevice, computePipeline, nullptr);
  vkDestroyPipelineLayout(logicalDevice, pipelineLayout, nullptr);
  vkDestroyDescriptorSetLayout(logicalDevice, imageDescriptorSetLayout, nullptr);
  vkResetDescriptorPool(logicalDevice, descriptorPool, 0);
  vkDestroyDescriptorPool(logicalDevice, descriptorPool, nullptr);
  vkDestroyDevice(logicalDevice, nullptr);
  vkDestroyInstance(instance, nullptr);
  dlclose(library);
}


} // namespace VkPerlin


using namespace VkPerlin;

int main(){
  Context appContext;
  appContext.run();
  appContext.shutdown();

  return 0;
}